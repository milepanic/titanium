<?php

use App\Item;
use Faker\Generator as Faker;

$factory->define(Item::class, function (Faker $faker) {
    $name = $faker->unique()->word;

    return [
        'image_sm' => 'proizvodi/elektromotor.jpg',
        'image_lg' => 'proizvodi/elektromotor.jpg',
        'name' => $name,
        'slug' => $name,
        'description' => $faker->paragraph,
        'category' => $faker->word,
        'type' => 'forklift',
    ];
});
