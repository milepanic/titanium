<?php

return [

    'email' => env('USER_EMAIL'),

    'password' => env('USER_PASSWORD'),

];
