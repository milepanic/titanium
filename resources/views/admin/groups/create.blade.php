@extends('lc-admin.layouts.master')
@section('title', __('New Group'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('New Group') }}</h3>
            
            <div class="ks-controls">
                {{ Breadcrumbs::render('lc-admin.groups.create') }}


                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 ks-panels-column-section">
                            <div class="card">
                                <div class="card-block">
                                    <h5 class="card-title">{{ __('New Group') }}</h5>
                                    <form action="{{ route('lc-admin.groups.store') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="name-input" class="col-sm-3 form-control-label">{{ __('Name') }}</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="name" class="form-control" id="name-input" placeholder="{{ __('Group name') }}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="logo-input" class="col-sm-3 form-control-label">{{ __('Logo') }}</label>
                                            <div class="col-sm-9">
                                                <label class="btn btn-primary ks-btn-file">
                                                    <span class="la la-cloud-upload ks-icon"></span>
                                                    <span class="ks-text">{{ __('Choose file') }}</span>
                                                    <input type="file" name="logo">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="description-input" class="col-sm-3 form-control-label">{{ __('Description') }}</label>
                                            <div class="col-sm-9">
                                                <textarea name="description" class="form-control" id="description-input" placeholder="{{ __('Group description') }}"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="pharmacy-input" class="col-sm-3 form-control-label">{{ __('Pharmacies') }}<span class="text-danger"> * </span></label>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="pharmacies[]" id="pharmacy-input" multiple>
                                                    <option selected disabled>{{ __('Select pharmacies') }}</option>
                                                    @foreach($pharmacies as $pharmacy)
                                                        <option value="{{ $pharmacy->id }}">{{ $pharmacy->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="user-input" class="col-sm-3 form-control-label">{{ __('Users') }}<span class="text-danger"> * </span></label>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="users[]" id="user-input" multiple>
                                                    <option selected disabled>{{ __('Select users') }}</option>
                                                    @foreach($users as $user)
                                                        <option value="{{ $user->id }}">{{ $user->first_name . ' ' . $user->last_name . ', ' . $user->email }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <button class="float-right btn btn-primary">{{ __('Submit') }}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
