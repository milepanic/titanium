<form action="{{ route('lc-admin.groups.destroy', [$group->id]) }}" method="POST" class="float-left">
   {{ method_field('DELETE') }}
   @csrf
   <button type="submit" title="Drop"><i class="ks-icon la la-remove"></i></button>
</form>
<a href="{{ route('lc-admin.groups.edit', ['groups' => $group->id]) }}" title="View"><i class="ks-icon la la-edit"></i></a>
