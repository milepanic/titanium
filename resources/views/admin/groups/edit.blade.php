@extends('lc-admin.layouts.master')
@section('title', __('Edit Group'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('Edit Group') }}</h3>
            
            <div class="ks-controls">
                {{ Breadcrumbs::render('lc-admin.groups.edit', $group) }}


                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 ks-panels-column-section">
                            <div class="card">
                                <div class="card-block">
                                    <h5 class="card-title">{{ __('Edit Group') }}</h5>
                                    <form action="{{ route('lc-admin.groups.update', ['group' => $group->id]) }}" method="POST" enctype="multipart/form-data">
                                        @method('PUT')
                                        @csrf
                                        <div class="form-group row">
                                            <label for="name-input" class="col-sm-3 form-control-label">{{ __('Name') }}</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="name" class="form-control" id="name-input" placeholder="{{ __('Group name') }}" value="{{ $group->name }}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="logo-input" class="col-sm-3 form-control-label">{{ __('Logo') }}</label>
                                            <div class="col-sm-9">
                                                <label class="btn btn-primary ks-btn-file">
                                                    <span class="la la-cloud-upload ks-icon"></span>
                                                    <span class="ks-text">{{ __('Choose file') }}</span>
                                                    <input type="file" name="logo">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="description-input" class="col-sm-3 form-control-label">{{ __('Description') }}</label>
                                            <div class="col-sm-9">
                                                <textarea name="description" class="form-control" id="description-input" placeholder="{{ __('Group description') }}">{{ $group->description }}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="user-input" class="col-sm-3 form-control-label">{{ __('Users') }}<span class="text-danger"> * </span></label>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="users[]" id="user-input" multiple>
                                                    @foreach($users as $user)
                                                        <option value="{{ $user->id }}" @if($group->users->contains($user)) selected @endif>{{ $user->first_name . ' ' . $user->last_name . ', ' . $user->email }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <button class="float-right btn btn-primary">{{ __('Edit') }}</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 ks-panels-column-section">
                            <div class="ks-body-wrap-container">
                                <div class="card">
                                    <div class="ks-full-table">
                                        <div class="ks-full-table-header">
                                            <h4 class="ks-full-table-name">{{ __('Pharmacies')}}</h4>
                                            <div class="ks-controls">
                                                <a href="{{ route('lc-admin.group-pharmacies.edit', ['group' => $group]) }}">
                                                    <button class="btn btn-primary">{{ __('Add Pharmacy')}}</button>
                                                </a>
                                            </div>
                                        </div>
                                        <table id="pharmacies-table" class="table ks-table-info dt-responsive nowrap">
                                            <thead>
                                                <tr>
                                                    <th width="1">ID </th>
                                                    <th>{{__('Name') }}</th>
                                                    <th>{{ __('Address') }}</th>
                                                    <th>{{ __('Location') }}</th>
                                                    <th>{{ __('Users') }}</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('external-js')
<script src="{{ asset('theme/lc-admin/libs/datatables-net/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/datatables-net/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/select2/js/select2.min.js') }}"></script>
<script type="application/javascript">
var DatatablesDataSourceAjaxServer = {
    init: function () {
        $("#pharmacies-table").DataTable({
            responsive: true, 
            searchDelay: 500, 
            processing: true, 
            serverSide: true, 
            ajax: "/lc-admin/group-pharmacies/" + window.location.pathname.split('/')[3], 
            columns: [
                { data: "id" }, 
                { 
                    data: "name",
                    render: function (data, type, row, meta) {
                        return "<a href='pharmacies/" + row.slug + "/edit'>" + data + "</a>";
                    }
                },
                { data: "address" }, 
                { data: "location.name" },
            ],
            columnDefs: [
                {
                    targets: 4,
                    render: function (data) {
                        users = [];
                        $.each(data, function (i, user) {
                            users.push("<a href='users/" + user.id + "/edit'>" + user.email + "</a>");
                        });

                        return users.join(', ');
                    }
                }
            ],
            "initComplete": function () {
                $('.dataTables_wrapper select').select2({
                    minimumResultsForSearch: Infinity
                });
            }
        })
    }
};

jQuery(document).ready(function () {
    DatatablesDataSourceAjaxServer.init();
});

</script>
@endsection
