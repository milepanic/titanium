@extends('admin.layouts.master')
@section('title', 'Svi proizvodi')

@section('external-css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>Svi proizvodi</h3>
            
            <div class="ks-controls">

                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-body-wrap-container">
                <div class="ks-full-table">
                    <div class="ks-full-table-header">
                        <h4 class="ks-full-table-name">Svi proizvodi</h4>
                        <div class="ks-controls">
                            <a href="{{ url('admin/dodavanje') }}">
                                <button class="btn btn-primary">Dodajte novi proizvod</button>
                            </a>
                        </div>
                    </div>
                    <table id="items-table" class="table ks-table-info dt-responsive nowrap">
                        <thead>
                            <tr>
                                <th width="1">ID </th>
                                <th>Ime</th>
                                <th>Tip</th>
                                <th>Akcija</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('external-js')
<script src="{{ asset('theme/lc-admin/libs/datatables-net/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/datatables-net/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/select2/js/select2.min.js') }}"></script>
<script type="application/javascript">var DatatablesDataSourceAjaxServer = {
    init: function () {
        $("#items-table").DataTable({
            responsive: true, 
            searchDelay: 500, 
            processing: true, 
            serverSide: true, 
            ajax: "/admin/proizvodi", 
            columns: [
                { data: "id" },
                { data: "name" },
                { data: "type" },
                { data: "action" }
            ],
            columnDefs: [
                {
                    targets: 2,
                    render: function (data) {
                        if (data === 'forklift') {
                            return 'viljuškar';
                        } else if (data === 'compressor') {
                            return 'kompresor';
                        }
                    }
                }
            ],
            "initComplete": function () {
                $('.dataTables_wrapper select').select2({
                    minimumResultsForSearch: Infinity
                });
            }
        })
    }
};

jQuery(document).ready(function () {
    DatatablesDataSourceAjaxServer.init();

    $('body').delegate('.delete-item', 'click', function (e) {
        e.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        if (confirm('Da li želite da obrišete ' + $(this).data('name') + '?')) {
            $.ajax({
                url: '/admin/proizvodi/' + $(this).data('slug'),
                type: 'DELETE',
            })
            .done(function() {
                alert('Obrisali ste proizvod');
            })
            .fail(function() {
                alert('Dogodila se greška');
            });
        }
    });
});

</script>
@endsection
