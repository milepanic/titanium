@extends('admin.layouts.master')
@section('title', 'Dodavanje')

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>Dodavanje proizovda</h3>
            
            <div class="ks-controls">
                {{-- {{ Breadcrumbs::render('lc-admin.groups.create') }} --}}


                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 ks-panels-column-section">
                            <div class="card">
                                <div class="card-block">
                                    <h5 class="card-title">Novi proizvod</h5>
                                    @if($errors->any())
                                        {{ implode('', $errors->all(':message ')) }}
                                    @endif
                                    <form action="{{ url('admin/dodavanje') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="name-input" class="col-sm-3 form-control-label">Naziv</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="name" class="form-control" id="name-input" placeholder="Naziv" value="{{ old('name') }}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="image-input" class="col-sm-3 form-control-label">Slika</label>
                                            <div class="col-sm-9">
                                                <label class="btn btn-primary ks-btn-file">
                                                    <span class="la la-cloud-upload ks-icon"></span>
                                                    <span class="ks-text">Odaberite sliku</span>
                                                    <input type="file" name="image">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="imageName-input" class="col-sm-3 form-control-label">Opis slike</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="imageName" class="form-control" id="imageName-input" placeholder="Kratak opis slike">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="description-input" class="col-sm-3 form-control-label">Opis</label>
                                            <div class="col-sm-9">
                                                <textarea name="description" class="form-control" id="description-input" placeholder="Opis (opcionalno)"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="type-input" class="col-sm-3 form-control-label">Tip</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="type" id="type-input">
                                                    <option selected disabled>Odaberite tip proizvoda</option>
                                                    <option value="forklift">Viljuškar</option>
                                                    <option value="compressor">Kompresor</option>
                                                </select>
                                            </div>
                                        </div>

                                        <button class="float-right btn btn-primary">Dodajte</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
