@extends('admin.layouts.master')
@section('title', 'Dodavanje slika')

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>Dodavanje slika</h3>
            
            <div class="ks-controls">
                {{-- {{ Breadcrumbs::render('lc-admin.groups.create') }} --}}


                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 ks-panels-column-section">
                            <div class="card">
                                <div class="card-block">
                                    <h5 class="card-title">Novi album</h5>
                                    @if($errors->any())
                                        {{ implode('', $errors->all(':message ')) }}
                                    @endif
                                    <form action="{{ url('admin/slike/dodavanje') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="name-input" class="col-sm-3 form-control-label">Naziv</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="name" class="form-control" id="name-input" placeholder="Naziv" value="{{ old('name') }}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="cover-input" class="col-sm-3 form-control-label">Naslovna slika</label>
                                            <div class="col-sm-9">
                                                <label class="btn btn-primary ks-btn-file">
                                                    <span class="la la-cloud-upload ks-icon"></span>
                                                    <span class="ks-text">Odaberite naslovnu sliku</span>
                                                    <input type="file" name="cover">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="coverAlt-input" class="col-sm-3 form-control-label">Opis naslovne slike</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="coverAlt" class="form-control" id="coverAlt-input" placeholder="Kratki opis naslovne slike" value="{{ old('coverAlt') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="images-input" class="col-sm-3 form-control-label">Slike</label>
                                            <div class="col-sm-9">
                                                <label class="btn btn-primary ks-btn-file">
                                                    <span class="la la-cloud-upload ks-icon"></span>
                                                    <span class="ks-text">Dodajte slike</span>
                                                    <input type="file" name="images[]" multiple>
                                                </label>
                                            </div>
                                        </div>
                                        <button class="float-right btn btn-primary">Napravite album</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
