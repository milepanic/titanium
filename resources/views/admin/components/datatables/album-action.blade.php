<div class="ks-controls">
    <a href="#" class="btn btn-danger ks-light ks-no-text delete-item" data-id="{{ $album->id }}" data-name="{{ $album->name }}" data-slug="{{ $album->slug }}" style="line-height: 38px;">
        <span class="la la-trash-o ks-icon"></span>
    </a>
</div>
