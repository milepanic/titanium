<div class="ks-controls">
    <a href="{{ url("admin/proizvodi/$item->slug/edit") }}" class="btn btn-primary ks-light ks-no-text" style="line-height: 38px;">
        <span class="la la-pencil ks-icon"></span>
    </a>
    <a href="#" class="btn btn-danger ks-light ks-no-text delete-item" data-id="{{ $item->id }}" data-name="{{ $item->name }}" data-slug="{{ $item->slug }}" style="line-height: 38px;">
        <span class="la la-trash-o ks-icon"></span>
    </a>
</div>
