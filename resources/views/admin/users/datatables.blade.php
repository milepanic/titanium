<form action="{{ route('lc-admin.users.destroy', [$user->id]) }}" method="POST" class="float-left">
   {{ method_field('DELETE') }}
   @csrf
   <button type="submit" title="Drop"><i class="ks-icon la la-remove"></i></button>
</form>
<a href="{{ route('lc-admin.users.edit', ['users' => $user->id]) }}" title="View"><i class="la la-edit"></i></a>
