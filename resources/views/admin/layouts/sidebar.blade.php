    <!-- BEGIN DEFAULT SIDEBAR -->
<div class="ks-column ks-sidebar ks-info">
    <div class="ks-wrapper ks-sidebar-wrapper">
        <ul class="nav nav-pills nav-stacked">

            <li class="nav-item">
                <a class="nav-link"  href="{{ url('admin') }}" >
                    <span class="ks-icon la la-dashboard"></span>
                    <span>Kontrolna tabla</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link"  href="{{ url('admin/dodavanje') }}" >
                    <span class="ks-icon la la-plus"></span>
                    <span>Dodavanje proizvoda</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link"  href="{{ url('admin/proizvodi') }}" >
                    <span class="ks-icon la la-list-ul"></span>
                    <span>Svi proizvodi</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link"  href="{{ url('admin/kontakt') }}" >
                    <span class="ks-icon la la-envelope"></span>
                <span>Poruke</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link"  href="{{ url('admin/slike') }}" >
                    <span class="ks-icon la la-image"></span>
                    <span>Albumi slika sa terena</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link"  href="{{ url('admin/slike/dodavanje') }}" >
                    <span class="ks-icon la la-camera"></span>
                    <span>Novi album</span>
                </a>
            </li>
        </ul>
        <div class="ks-sidebar-extras-block">
            <div class="ks-sidebar-copyright">2018 &copy; Admin panel  by <a href="https://laracode.net" class="m-link">Laracode.net</a></div>
        </div>
    </div>
</div>
<!-- END DEFAULT SIDEBAR -->
