@extends('layouts.master')
@section('title')
    <title>Titanium Servis</title>
    <meta name="description" content="160">
    <link rel="canonical" href="{{ url('/') }}" />

    <meta property="og:type" content="article" />
    <meta property="og:title" content="naslov" />
    <meta property="og:description" content="opis" />
    <meta property="og:image" content="LINK TO THE IMAGE FILE" />
    <meta property="og:url" content="{{ Request::url() }}" />
@endsection
@section('content')

<div id="minimal-bootstrap-carousel" class="carousel slide carousel-fade slider-home-one" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active slide-1" style="background-image: url(img/viljuskar-8-dark.jpg);background-position: center center;">

            <div class="carousel-caption">
                <div class="container">
                    <div class="box valign-middle">
                        <div class="content text-center">
                            <h3 data-animation="animated fadeInUp">Dobrodošli</h3>
                            <h2 data-animation="animated fadeInUp">Titanium Servis</h2>
                            <p data-animation="animated fadeInDown">Sve za kompresore i viljuškare</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item slide-2" style="background-image: url(img/alat.jpg);background-position: center center;">

            <div class="carousel-caption">
                <div class="container">
                    <div class="box valign-middle">
                        <div class="content text-center">
                            <h3 data-animation="animated fadeInUp">Raznovrsne usluge</h3>
                            <h2 data-animation="animated fadeInUp">Popravljamo viljuškare i kompresore</h2>
                            <p data-animation="animated fadeInDown">Imate kvar? Trebaju li vam dijelovi ili stručno osoblje?</p>
                            <a href="{{ url('kontakt') }}" class="banner-btn" data-animation="animated fadeInDown">Kontaktirajte nas</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item slide-3" style="background-image: url(img/viljuskar-9-dark.jpg);background-position: center center;">

            <div class="carousel-caption">
                <div class="container">
                    <div class="box valign-middle">
                        <div class="content text-center">
                            <h3 data-animation="animated fadeInUp">Remont, servis i održavanje<h3>
                            <h2 data-animation="animated fadeInUp">Svih vrsta viljuškara</h2>
                            <p data-animation="animated fadeInDown">Hidraulika, mehanika, elektronika, mehatronika</p>
                            <a href="{{ url('viljuškari') }}" class="banner-btn" data-animation="animated fadeInDown">Pogledajte ponudu</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item slide-4" style="background-image: url(img/viljuskar-9-dark.jpg);background-position: center center;">

            <div class="carousel-caption">
                <div class="container">
                    <div class="box valign-middle">
                        <div class="content text-center">
                            <h3 data-animation="animated fadeInUp">Remont, servis i održavanje</h3>
                            <h2 data-animation="animated fadeInUp">Svih vrsta kompresora</h2>
                            <p data-animation="animated fadeInDown">Vičanih, klipnih, rotacionih</p>
                            <a href="{{ url('kompresori') }}" class="banner-btn" data-animation="animated fadeInDown">Pogledajte ponudu</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Controls -->
    <a class="left carousel-control" href="#minimal-bootstrap-carousel" role="button" data-slide="prev">
        <i class="fas fa-angle-left"></i>
        <span class="sr-only">Prethodni</span>
    </a>
    <a class="right carousel-control" href="#minimal-bootstrap-carousel" role="button" data-slide="next">
        <i class="fas fa-angle-right"></i>
        <span class="sr-only">Sledeći</span>
    </a>

    <ul class="carousel-indicators list-inline custom-navigation">
        <li data-target="#minimal-bootstrap-carousel" data-slide-to="0" class="active"></li>
        <li data-target="#minimal-bootstrap-carousel" data-slide-to="1"></li>
        <li data-target="#minimal-bootstrap-carousel" data-slide-to="2"></li>
        <li data-target="#minimal-bootstrap-carousel" data-slide-to="3"></li>
    </ul>
</div>


<section class="service-style-one sec-pad">
    <div class="container">
        <div class="sec-title">
            <div class="row">
                <div class="col-md-3">
                    <h3>Šta radimo</h3>
                </div><!-- /.col-md-3 -->
                <div class="col-md-6">
                    <p>
                        Vršimo usluge remonta, servisiranja i popravki <br />
                        svih vrsta viljuškara i kompresora
                    </p>
                </div><!-- /.col-md-6 -->
                <div class="col-md-3 text-right">
                    <a href="{{ url('usluge') }}" class="thm-btn bordered">Sve usluge</a>
                </div><!-- /.col-md-3 -->
            </div><!-- /.row -->
        </div><!-- /.sec-title -->
        <div class="service-carousel-one owl-theme owl-carousel">
            <div class="item">
                <div class="single-service-style-one">
                    <img src="img/servis-agregat2.jpg" alt="Benzinski agregat"/>
                    {{-- <div class="img-box">
                        <img src="img/servis-agregat2.jpg" alt="Benzinski agregat"/>
                        <a href="#" class="read-more fas fa-link"></a>
                    </div><!-- /.img-box --> --}}
                    <div class="content-box">
                        {{-- <div class="icon-box">
                            <i class="zxp-icon-insurance"></i>
                        </div><!-- /.icon-box --> --}}
                        <div class="text-box">
                            <h3>Popravka benzinskih i dizel agregata</h3>
                            {{-- <a href="#"><h3>Popravka benzinskih i dizel agregata</h3></a> --}}
                        </div><!-- /.text-box -->
                    </div><!-- /.content-box -->
                </div><!-- /.single-service-style-one -->
            </div><!-- /.item -->
            <div class="item">
                <div class="single-service-style-one">
                    <img src="img/servis-hidropogon2.jpg" alt="Pumpa za hidro pogon"/>
                    {{-- <div class="img-box">
                        <img src="img/servis-hidropogon2.jpg" alt="Pumpa za hidro pogon"/>
                        <a href="#" class="read-more fas fa-link"></a>
                    </div><!-- /.img-box --> --}}
                    <div class="content-box">
                        {{-- <div class="icon-box">
                            <i class="zxp-icon-skyline"></i>
                        </div><!-- /.icon-box --> --}}
                        <div class="text-box">
                            <h3>Popravka hidro pogona</h3>
                            {{-- <a href="#"><h3>Popravka hidro pogona</h3></a> --}}
                        </div><!-- /.text-box -->
                    </div><!-- /.content-box -->
                </div><!-- /.single-service-style-one -->
            </div><!-- /.item -->
            <div class="item">
                <div class="single-service-style-one">
                    <img src="img/servis-elektromotor.jpg" alt="Elektromotor"/>
                    {{-- <div class="img-box">
                        <img src="img/servis-elektromotor.jpg" alt="Elektromotor"/>
                        <a href="#" class="read-more fas fa-link"></a>
                    </div><!-- /.img-box --> --}}
                    <div class="content-box">
                        {{-- <div class="icon-box">
                            <i class="zxp-icon-couch"></i>
                        </div><!-- /.icon-box --> --}}
                        <div class="text-box">
                            <h3>Popravka elektromotora i generatora</h3>
                            {{-- <a href="#"><h3>Popravka elektromotora i generatora</h3></a> --}}
                        </div><!-- /.text-box -->
                    </div><!-- /.content-box -->
                </div><!-- /.single-service-style-one -->
            </div><!-- /.item -->
            <div class="item">
                <div class="single-service-style-one">
                    <img src="img/servis-plin.jpg" alt="Ugradnja plina"/>
                    {{-- <div class="img-box">
                        <img src="img/servis-plin.jpg" alt="Ugradnja plina"/>
                        <a href="#" class="read-more fas fa-link"></a>
                    </div><!-- /.img-box --> --}}
                    <div class="content-box">
                        {{-- <div class="icon-box">
                            <i class="zxp-icon-plan"></i>
                        </div><!-- /.icon-box --> --}}
                        <div class="text-box">
                            <h3>Ugradnja i servisiranje plina</h3>
                            {{-- <a href="#"><h3>Ugradnja i servisiranje plina</h3></a> --}}
                        </div><!-- /.text-box -->
                    </div><!-- /.content-box -->
                </div><!-- /.single-service-style-one -->
            </div><!-- /.item -->
            <div class="item">
                <div class="single-service-style-one">
                    <img src="img/servis-elektronika.jpg" alt="Elektronika"/>
                    {{-- <div class="img-box">
                        <img src="img/servis-elektronika.jpg" alt="Elektronika"/>
                        <a href="#" class="read-more fas fa-link"></a>
                    </div><!-- /.img-box --> --}}
                    <div class="content-box">
                        {{-- <div class="icon-box">
                            <i class="zxp-icon-wallpaper"></i>
                        </div><!-- /.icon-box --> --}}
                        <div class="text-box">
                            <h3>Popravka elektronike</h3>
                            {{-- <a href="#"><h3>Popravka elektronike</h3></a> --}}
                        </div><!-- /.text-box -->
                    </div><!-- /.content-box -->
                </div><!-- /.single-service-style-one -->
            </div><!-- /.item -->
            <div class="item">
                <div class="single-service-style-one">
                    <img src="img/mehatronika.png" alt="Mehatronika"/>
                    {{-- <div class="img-box">
                        <img src="img/mehatronika.png" alt="Mehatronika"/>
                        <a href="#" class="read-more fas fa-link"></a>
                    </div><!-- /.img-box --> --}}
                    <div class="content-box">
                        {{-- <div class="icon-box">
                            <i class="zxp-icon-stairs"></i>
                        </div><!-- /.icon-box --> --}}
                        <div class="text-box">
                            <h3>Popravka mehatronike</h3>
                            {{-- <a href="#"><h3>Popravka mehatronike</h3></a> --}}
                        </div><!-- /.text-box -->
                    </div><!-- /.content-box -->
                </div><!-- /.single-service-style-one -->
            </div><!-- /.item -->
            <div class="item">
                <div class="single-service-style-one">
                    <img src="img/servis-hidraulika.jpg" alt="Hidraulika"/>
                    {{-- <div class="img-box">
                        <img src="img/servis-hidraulika.jpg" alt="Hidraulika"/>
                        <a href="#" class="read-more fas fa-link"></a>
                    </div><!-- /.img-box --> --}}
                    <div class="content-box">
                        {{-- <div class="icon-box">
                            <i class="zxp-icon-insurance"></i>
                        </div><!-- /.icon-box --> --}}
                        <div class="text-box">
                            <h3>Popravka hidraulike</h3>
                            {{-- <a href="#"><h3>Popravka hidraulike</h3></a> --}}
                        </div><!-- /.text-box -->
                    </div><!-- /.content-box -->
                </div><!-- /.single-service-style-one -->
            </div><!-- /.item -->
            <div class="item">
                <div class="single-service-style-one">
                    <img src="img/servis-dijagnostika.jpg" alt="Alat za dijagnostiku viljuškara"/>
                    {{-- <div class="img-box">
                        <img src="img/servis-dijagnostika.jpg" alt="Alat za dijagnostiku viljuškara"/>
                        <a href="#" class="read-more fas fa-link"></a>
                    </div><!-- /.img-box --> --}}
                    <div class="content-box">
                        {{-- <div class="icon-box">
                            <i class="zxp-icon-skyline"></i>
                        </div><!-- /.icon-box --> --}}
                        <div class="text-box">
                            <h3>Dijagnostika za Linda viljuškare</h3>
                            {{-- <a href="#"><h3>Dijagnostika za Linda viljuškare</h3></a> --}}
                        </div><!-- /.text-box -->
                    </div><!-- /.content-box -->
                </div><!-- /.single-service-style-one -->
            </div><!-- /.item -->
            <div class="item">
                <div class="single-service-style-one">
                    <img src="img/viljuskar-servis.jpg" alt="Viljuškar u magacinu"/>
                    {{-- <div class="img-box">
                        <img src="img/viljuskar-servis.jpg" alt="Viljuškar u magacinu"/>
                        <a href="#" class="read-more fas fa-link"></a>
                    </div><!-- /.img-box --> --}}
                    <div class="content-box">
                        {{-- <div class="icon-box">
                            <i class="zxp-icon-couch"></i>
                        </div><!-- /.icon-box --> --}}
                        <div class="text-box">
                            <h3>Nabavka i prodaja svih dijelova za viljuškare</h3>
                            {{-- <a href="#"><h3>Nabavka i prodaja svih dijelova za viljuškare</h3></a> --}}
                        </div><!-- /.text-box -->
                    </div><!-- /.content-box -->
                </div><!-- /.single-service-style-one -->
            </div><!-- /.item -->
            <div class="item">
                <div class="single-service-style-one">
                    <img src="img/servis-kompresor1.jpg" alt="Zračni kompresor"/>
                    {{-- <div class="img-box">
                        <img src="img/servis-kompresor1.jpg" alt="Zračni kompresor"/>
                        <a href="#" class="read-more fas fa-link"></a>
                    </div><!-- /.img-box --> --}}
                    <div class="content-box">
                        {{-- <div class="icon-box">
                            <i class="zxp-icon-skyline"></i>
                        </div><!-- /.icon-box --> --}}
                        <div class="text-box">
                            <h3>Popravka zračnih kompresora</h3>
                            {{-- <a href="#"><h3>Popravka zračnih kompresora</h3></a> --}}
                            <p>vicanih, rotacionih, klipnih</p>
                        </div><!-- /.text-box -->
                    </div><!-- /.content-box -->
                </div><!-- /.single-service-style-one -->
            </div><!-- /.item -->
            <div class="item">
                <div class="single-service-style-one">
                    <img src="img/servis-kompresor2.png" alt="Dio kompresora"/>
                    {{-- <div class="img-box">
                        <img src="img/servis-kompresor2.png" alt="Dio kompresora"/>
                        <a href="#" class="read-more fas fa-link"></a>
                    </div><!-- /.img-box --> --}}
                    <div class="content-box">
                        {{-- <div class="icon-box">
                            <i class="zxp-icon-skyline"></i>
                        </div><!-- /.icon-box --> --}}
                        <div class="text-box">
                            <h3>Dijelovi za kompresore</h3>
                            {{-- <a href="#"><h3>Dijelovi za kompresore</h3></a> --}}
                        </div><!-- /.text-box -->
                    </div><!-- /.content-box -->
                </div><!-- /.single-service-style-one -->
            </div><!-- /.item -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.service-style-one sec-pad -->

@include('components.map')

<section class="about-style-two">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <img src="img/about-feature-1-1.jpg" class="pull-right" alt=""/>
            </div><!-- /.col-md-5 -->
            <div class="col-md-7">
                <div class="about-content">
                    <span>O Nama</span>
                    <h1>Titanium servis</h1>
                    <p>je mlada firma sa mnogo iskusnih individualaca i osnovana je zbog potrebe tržišta za ozbiljnim servisom i kvalitetnom uslugom, a prije svega brzom intervencijom, profesionalnom odnosu prema komitentima i po našem mišljenju najvažnije od svega rešavanjem kvarova na licu mjesta i intervencijom u roku 24 časa bez dodatnih troškova i gubljenja vremena na transport mašina i dijelova zbog većih kvarova kao što je slučaj sa većinom ostalih konkurenata na tržištu.</p>
                </div><!-- /.about-content -->
            </div><!-- /.col-md-7 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.about-style-two -->

<section class="project-style-one sec-pad pb0">
    <div class="container-fluid">
        <div class="sec-title text-center">
            <h3>Slike sa terena</h3>
            <p>Albumi naših slika sa terena</p>
        </div>
        <div class="row masonary-layout filter-layout" data-filter-class="filter">
            @foreach ($albums as $album)
                <div class="col-md-3 col-sm-6 col-xs-12 masonary-item single-filter-item">
                    <div class="single-project-style-one">
                        <div class="img-box">
                            <div class="img-box-images" hidden>
                                @foreach (json_decode($album->images) as $image)
                                    <div class="img-box-image" data-image="{{ asset($image) }}"></div>
                                @endforeach
                            </div>
                            <img src="{{ asset($album->cover) }}" alt="{{ $album->coverAlt }}"/>
                            <div class="overlay">
                                <div class="box">
                                    <div class="content">
                                        <a href="#" class="more-btn fas fa-images"></a>
                                        <h3>{{ $album->name }}</h3>
                                    </div><!-- /.content -->
                                </div><!-- /.box -->
                            </div><!-- /.overlay -->
                        </div><!-- /.img-box -->
                    </div><!-- /.single-project-style-one -->
                </div>
            @endforeach
        </div>
    </div><!-- /.container -->
</section><!-- /.project-style-one -->

<section class="cta-style-one">
    <div class="inner">
        <div class="container">
            <div class="title pull-left">
                <h3>Ponosno radimo ono što znamo najbolje.</h3>
            </div><!-- /.title pull-left -->
            <div class="btn-box pull-right">
                <a href="{{ url('albumi') }}" class="cta-btn">Svi projekti</a>
            </div><!-- /.btn-box -->
        </div><!-- /.container -->
    </div><!-- /.inner -->
</section><!-- /.cta-style-one -->

<div class="brand-carousel-wrapper">
    <div class="container">
        <div class="brand-carousel owl-carousel owl-theme">
            <div class="item">
                <i class="brands-icon-audiojungle"></i>
            </div><!-- /.item -->
            <div class="item">
                <i class="brands-icon-codecanyon"></i>
            </div><!-- /.item -->
            <div class="item">
                <i class="brands-icon-envato"></i>
            </div><!-- /.item -->
            <div class="item">
                <i class="brands-icon-graphicriver"></i>
            </div><!-- /.item -->
            <div class="item">
                <i class="brands-icon-photodune"></i>
            </div><!-- /.item -->
            <div class="item">
                <i class="brands-icon-themeforest"></i>
            </div><!-- /.item -->
            <div class="item">
                <i class="brands-icon-videohive"></i>
            </div><!-- /.item -->
        </div><!-- /.brand-carousel -->
    </div><!-- /.container -->
</div><!-- /.brand-carousel-wrapper -->

{{-- <section class="blog-style-two sec-pad">
    <div class="container">
        <div class="sec-title">
            <div class="row">
                <div class="col-md-3">
                    <h3>Naš blog</h3>
                </div><!-- /.col-md-3 -->
                <div class="col-md-6">
                    <p>Pročitajte i neke od naših tekstova</p>
                </div><!-- /.col-md-6 -->
                <div class="col-md-3 text-right">
                    <a href="#" class="thm-btn">Sve objave</a>
                </div><!-- /.col-md-3 -->
            </div><!-- /.row -->
        </div><!-- /.sec-title -->
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="single-blog-style-two">
                    <div class="img-box">
                        <img src="img/blog-1-1.jpg" alt=""/>
                        <a href="blog-details.html" class="read-more">Read Post</a>
                    </div><!-- /.img-box -->
                    <div class="text-box">
                        <div class="meta-info">
                            <a href="#"><i class="fas fa-clock"></i> Nov 25</a>
                        </div><!-- /.meta-info -->
                        <a href="blog-details.html"><h3>2018 Project: Planning Anduff  Construction</h3></a>
                    </div><!-- /.text-box -->
                </div><!-- /.single-blog-style-two -->
            </div><!-- /.col-md-4 -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="single-blog-style-two">
                    <div class="img-box">
                        <img src="img/blog-1-2.jpg" alt=""/>
                        <a href="blog-details.html" class="read-more">Read Post</a>
                    </div><!-- /.img-box -->
                    <div class="text-box">
                        <div class="meta-info">
                            <a href="#"><i class="fas fa-clock"></i> Nov 25</a>
                        </div><!-- /.meta-info -->
                        <a href="blog-details.html"><h3>Central Valley Comes Calling with Industrial Options</h3></a>
                    </div><!-- /.text-box -->
                </div><!-- /.single-blog-style-two -->
            </div><!-- /.col-md-4 -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="single-blog-style-two">
                    <div class="img-box">
                        <img src="img/blog-1-3.jpg" alt=""/>
                        <a href="blog-details.html" class="read-more">Read Post</a>
                    </div><!-- /.img-box -->
                    <div class="text-box">
                        <div class="meta-info">
                            <a href="#"><i class="fas fa-clock"></i> Nov 25</a>
                        </div><!-- /.meta-info -->
                        <a href="blog-details.html"><h3>Columbus' Industrial Market Continues to Rise</h3></a>
                    </div><!-- /.text-box -->
                </div><!-- /.single-blog-style-two -->
            </div><!-- /.col-md-4 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.blog-style-two --> --}}
@endsection
@section('external-js')
<script>
$(document).ready(function () {
    $('.img-box').each(function () {
        var gallery = [];

        $(this).find('.img-box-image').each(function (i) {
            var src = $(this).data('image');

            image = {};
            image["src"] = src;
            image["type"] = 'image';

            gallery.push(image);
        });

        $(this).magnificPopup({
            items: gallery,
            gallery: {
                enabled: true
            },
        });
    });
});
</script>
@endsection
