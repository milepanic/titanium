<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="robots" content="index, follow">
    <meta property="fb:app_id" content="">
    <meta property="og:locale" content="sr" />
    @yield('title')
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="theme-color" content="#EC1A23">
    {{-- <link rel="stylesheet" href="{{ asset('css/style.css') }}"> --}}
    {{-- <link rel="stylesheet" href="{{ asset('css/responsive.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('css/stylesheet.min.css') }}">

    <script src='https://api.mapbox.com/mapbox-gl-js/v0.51.0/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v0.51.0/mapbox-gl.css' rel='stylesheet' />
</head>
<body class="active-preloader-ovh">

<div class="preloader"><div class="spinner"></div></div> <!-- /.preloader -->

@include('components.header')

@yield('content')

@include('components.footer')

<div class="search_area zoom-anim-dialog mfp-hide" id="test-search">
    <div class="search_box_inner">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fas fa-search"></i></button>
            </span>
        </div>
    </div>
</div>

{{-- <div class="scroll-to-top scroll-to-target" data-target="html"><i class="fa fa-angle-up"></i></div> --}}

<script src="{{ asset('js/jquery.js') }}"></script>

<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/isotope.js') }}"></script>
<script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('js/waypoints.min.js') }}"></script>
<script src="{{ asset('js/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('js/wow.min.js') }}"></script>
<script src="{{ asset('js/jquery.easing.min.js') }}"></script>
{{-- <script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script> --}}
<script src="{{ asset('js/custom.js') }}"></script>
@yield('external-js')

</body>
</html>
