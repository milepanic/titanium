@extends('layouts.master')
@section('title')
    @if(request()->is('viljuškari'))
        <title>Viljuškari | Titanium Servis</title>
        <meta name="description" content="160">
        <link rel="canonical" href="{{ url('viljuškari') }}" />

        <meta property="og:type" content="article" />
        <meta property="og:title" content="naslov" />
        <meta property="og:description" content="opis" />
        <meta property="og:image" content="LINK TO THE IMAGE FILE" />
        <meta property="og:url" content="{{ Request::url() }}" />
    @elseif(request()->is('kompresori'))
        <title>Kompresori | Titanium Servis</title>
        <meta name="description" content="160">
        <link rel="canonical" href="{{ url('kompresori') }}" />

        <meta property="og:type" content="article" />
        <meta property="og:title" content="naslov" />
        <meta property="og:description" content="opis" />
        <meta property="og:image" content="LINK TO THE IMAGE FILE" />
        <meta property="og:url" content="{{ Request::url() }}" />
    @endif
@endsection
@section('content')

<section class="inner-banner">
    <div class="container text-center">
        <h3>@if(\Request::is('viljuškari')) Viljuškari @else Kompresori @endif</h3>
        <div class="breadcumb">
            <a href="{{ url('/') }}">Početna</a><!--
            --><span class="sep">-</span><!--
            --><span class="page-name">@if(\Request::is('viljuškari')) Viljuškari @else Kompresori @endif</span>
        </div><!-- /.breadcumb -->
    </div><!-- /.container -->
</section><!-- /.inner-banner -->

<section class="service-style-one sec-pad home-page-three">
    <div class="container">
        <div class="sec-title">
            <div class="row">
                <div class="col-md-12">
                    @if(\Request::is('viljuškari'))
                        <h3 class="text-center">Dijelovi za viljuškare</h3>
                        <p class="text-center">
                            prodajemo ovo narucite od nas
                        </p>
                    @elseif(\Request::is('kompresori'))
                        <h3 class="text-center">Dijelovi za kompresore</h3>
                        <p class="text-center">
                            prodajemo ovo narucite od nas
                        </p>
                    @endif
                </div>
            </div><!-- /.row -->
        </div><!-- /.sec-title -->
        <div class="row">
            @foreach($items->chunk(3) as $chunk)
                <div class="row">
                    @foreach($chunk as $item)

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-service-style-one">
                                <div class="img-box">
                                    <img src="{{ asset($item->image_sm) }}" alt="{{ $item->image_name }}">
                                    <a href="{{ url($item->type === 'forklift' ? 'viljuškari/'. $item->slug : 'kompresori/'. $item->slug) }}" class="read-more fas fa-link"></a>
                                </div><!-- /.img-box -->
                                <div class="content-box">
                                    <div class="text-box">
                                        <a href="{{ url($item->type === 'forklift' ? 'viljuškari/'. $item->slug : 'kompresori/'. $item->slug) }}">
                                            <h3>{{ $item->name }}</h3>
                                        </a>
                                    </div><!-- /.text-box -->
                                </div><!-- /.content-box -->
                            </div><!-- /.single-service-style-one -->
                        </div><!-- /.col-md-4 -->

                    @endforeach
                </div>
            @endforeach
            <div>
                {{ $items->links() }}
            </div>
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>

@endsection
