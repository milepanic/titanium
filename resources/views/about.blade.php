@extends('layouts.master')
@section('title')
    <title>O Nama | Titanium Servis</title>
    <meta name="description" content="160">
    <link rel="canonical" href="{{ url('o-nama') }}" />

    <meta property="og:type" content="article" />
    <meta property="og:title" content="naslov" />
    <meta property="og:description" content="opis" />
    <meta property="og:image" content="LINK TO THE IMAGE FILE" />
    <meta property="og:url" content="{{ Request::url() }}" />
@endsection
@section('content')

<section class="inner-banner">
    <div class="container text-center">
        <h3>O Nama</h3>
        <div class="breadcumb">
            <a href="{{ url('/') }}">Početna</a><!--
            --><span class="sep">-</span><!--
            --><span class="page-name">O Nama</span>
        </div><!-- /.breadcumb -->
    </div><!-- /.container -->
</section><!-- /.inner-banner -->

<section class="about-us-style-one sec-pad">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="about-content">
                    <span>O Nama</span>
                    <h3>Titanium servis</h3>
                    <p>je mlada firma sa mnogo iskusnih individualaca i osnovana je zbog potrebe tržišta za ozbiljnim servisom i kvalitetnom uslugom, a prije svega brzom intervencijom, profesionalnom odnosu prema komitentima i po našem mišljenju najvažnije od svega rešavanjem kvarova na licu mjesta i intervencijom u roku 24 časa bez dodatnih troškova i gubljenja vremena na transport mašina i dijelova zbog većih kvarova kao što je slučaj sa većinom ostalih konkurenata na tržištu.</p>
                    <a href="{{ url('usluge') }}" class="about-btn">Sve usluge</a>
                </div><!-- /.about-content -->
            </div><!-- /.col-md-6 -->
            <div class="col-md-6 text-right">
                <img src="img/about-1-1.png" alt="Awesome Image"/>
            </div><!-- /.col-md-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.about-us-style-one -->

@include('components.map')


<section class="cta-style-one">
    <div class="inner">
        <div class="container">
            <div class="title pull-left">
                <h3>Ponosno radimo ono što znamo najbolje.</h3>
            </div><!-- /.title pull-left -->
            <div class="btn-box pull-right">
                <a href="#" class="cta-btn">Svi projekti</a>
            </div><!-- /.btn-box -->
        </div><!-- /.container -->
    </div><!-- /.inner -->
</section><!-- /.cta-style-one -->

{{-- <section class="testimonials-feature-wrapper sec-pad">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="testimonials-style-one">
                    <div class="sec-title">
                        <h3>Clients’ Review</h3>
                    </div><!-- /.sec-title -->
                    <div class="single-testimonial-style-one">
                        <div class="top-box">
                            <i class="qoute-icon zxp-icon-right-quote"></i>
                            <div class="icon-box">
                                <img src="img/testi-1-1.jpg" alt="Awesome Image"/>
                            </div><!-- /.icon-box -->
                            <div class="text-box">
                                <h3>Outstanding Quality</h3>
                                <div class="stars">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div><!-- /.stars -->
                            </div><!-- /.text-box -->
                        </div><!-- /.top-box -->
                        <div class="content-box">
                            <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, dipsi plotum <br /> con sectetur, adipisci velit, sed quia non num quam eius modi tem amet conte <br /> pora incid unt ut labore et dolore magnam.</p>
                            <h4>- Ida Leopard, Google</h4>
                        </div><!-- /.content-box -->
                    </div><!-- /.single-testimonial-style-one -->
                    <div class="single-testimonial-style-one">
                        <div class="top-box">
                            <i class="qoute-icon zxp-icon-right-quote"></i>
                            <div class="icon-box">
                                <img src="img/testi-1-2.jpg" alt="Awesome Image"/>
                            </div><!-- /.icon-box -->
                            <div class="text-box">
                                <h3>Professional Handling</h3>
                                <div class="stars">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div><!-- /.stars -->
                            </div><!-- /.text-box -->
                        </div><!-- /.top-box -->
                        <div class="content-box">
                            <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, dipsi plotum <br /> con sectetur, adipisci velit, sed quia non num quam eius modi tem amet conte <br /> pora incid unt ut labore et dolore magnam.</p>
                            <h4>- Jackelyn Fernendez, Twitter</h4>
                        </div><!-- /.content-box -->
                    </div><!-- /.single-testimonial-style-one -->
                    <div class="btn-box">
                        <a href="#" class="view-more">View More</a>
                    </div><!-- /.btn-box -->
                </div><!-- /.testimonials-style-one -->
            </div><!-- /.col-md-6 -->
            <div class="col-md-6">
                <div class="feature-style-one">
                    <div class="sec-title">
                        <h3>Our Strength</h3>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem acc usantium doloremque laudantium, totam rem aperiam, eaque ipsa quae. Ab illo inventore veritatis et quasi archite.</p>
                    </div><!-- /.sec-title -->
                    <div class="single-feature-style-one">
                        <div class="icon-box">
                            <i class="zxp-icon-pumpjack"></i>
                        </div><!-- /.icon-box -->
                        <div class="text-box">
                            <h3 class="large-font">Advanced Technology</h3>
                            <p>Sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam ali quam quaerat voluptatem. Lorem ipsum dolor sit amet, </p>
                        </div><!-- /.text-box -->
                    </div><!-- /.single-feature-style-one -->
                    <div class="single-feature-style-one">
                        <div class="icon-box">
                            <i class="zxp-icon-architect-with-helmet"></i>
                        </div><!-- /.icon-box -->
                        <div class="text-box">
                            <h3 class="large-font">Expert Team</h3>
                            <p>Sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam ali quam quaerat voluptatem. Lorem ipsum dolor sit amet, </p>
                        </div><!-- /.text-box -->
                    </div><!-- /.single-feature-style-one -->
                    <div class="single-feature-style-one">
                        <div class="icon-box">
                            <i class="zxp-icon-time-passing"></i>
                        </div><!-- /.icon-box -->
                        <div class="text-box">
                            <h3 class="large-font">On time Delivery</h3>
                            <p>Sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam ali quam quaerat voluptatem. Lorem ipsum dolor sit amet, </p>
                        </div><!-- /.text-box -->
                    </div><!-- /.single-feature-style-one -->
                </div><!-- /.feature-style-one -->
            </div><!-- /.col-md-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.testimonials-feature-wrapper --> --}}

@endsection
