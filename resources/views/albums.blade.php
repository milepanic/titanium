@extends('layouts.master')
@section('title')
    <title>Slike sa terena | Titanium Servis</title>
    <meta name="description" content="160">
    <link rel="canonical" href="{{ url('albumi') }}" />

    <meta property="og:type" content="article" />
    <meta property="og:title" content="naslov" />
    <meta property="og:description" content="opis" />
    <meta property="og:image" content="LINK TO THE IMAGE FILE" />
    <meta property="og:url" content="{{ Request::url() }}" />
@endsection
@section('content')
<section class="inner-banner">
    <div class="container text-center">
        <h3>Slike sa terena</h3>
        <div class="breadcumb">
            <a href="{{ url('/') }}">Početna</a><!--
            --><span class="sep">-</span><!--
            --><span class="page-name">Slike sa terena</span>
        </div><!-- /.breadcumb -->
    </div><!-- /.container -->
</section><!-- /.inner-banner -->

<section class="project-style-one sec-pad">
    <div class="container-fluid">
        <div class="sec-title text-center">
            <h3>Slike sa terena</h3>
            <p>Albumi naših slika sa terena</p>
        </div>
        <div class="row masonary-layout filter-layout" data-filter-class="filter">
            @foreach ($albums as $album)
                <div class="col-md-3 col-sm-6 col-xs-12 masonary-item single-filter-item">
                    <div class="single-project-style-one">
                        <div class="img-box">
                            <div class="img-box-images" hidden>
                                @foreach (json_decode($album->images) as $image)
                                    <div class="img-box-image" data-image="{{ asset($image) }}"></div>
                                @endforeach
                            </div>
                            <img src="{{ asset($album->cover) }}" alt="{{ $album->coverAlt }}"/>
                            <div class="overlay">
                                <div class="box">
                                    <div class="content">
                                        <a href="#" class="more-btn fas fa-link"></a>
                                        <h3>{{ $album->name }}</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        {{ $albums->links() }}
    </div><!-- /.container -->
</section>
@endsection
@section('external-js')
<script>
$(document).ready(function () {
    $('.img-box').each(function () {
        var gallery = [];

        $(this).find('.img-box-image').each(function (i) {
            var src = $(this).data('image');

            image = {};
            image["src"] = src;
            image["type"] = 'image';

            gallery.push(image);
        });

        $(this).magnificPopup({
            items: gallery,
            gallery: {
                enabled: true
            },
        });
    });
});
</script>
@endsection
