@extends('layouts.master')
@section('title')
    <title>Usluge | Titanium Servis</title>
    <meta name="description" content="160">
    <link rel="canonical" href="{{ url('usluge') }}" />

    <meta property="og:type" content="article" />
    <meta property="og:title" content="naslov" />
    <meta property="og:description" content="opis" />
    <meta property="og:image" content="LINK TO THE IMAGE FILE" />
    <meta property="og:url" content="{{ Request::url() }}" />
@endsection
@section('content')

<section class="inner-banner">
    <div class="container text-center">
        <h3>Usluge</h3>
        <div class="breadcumb">
            <a href="{{ url('/') }}">Početna</a><!--
            --><span class="sep">-</span><!--
            --><span class="page-name">Usluge</span>
        </div><!-- /.breadcumb -->
    </div><!-- /.container -->
</section><!-- /.inner-banner -->

<section class="service-style-one sec-pad service-page">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="single-service-style-one">
                    <div class="img-box">
                        <img src="{{ asset('img/servis-agregat2.jpg') }}" alt="Benzinski agregat"/>
                        {{-- <a href="#" class="read-more fas fa-link"></a> --}}
                    </div>
                    <div class="content-box">
                        <div class="text-box">
                            <h3>Popravka benzinskih i dizel agregata</h3>
                        </div><!-- /.text-box -->
                    </div><!-- /.content-box -->
                </div><!-- /.single-service-style-one -->
            </div><!-- /.col-md-4 -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="single-service-style-one">
                    <div class="img-box">
                        <img src="{{ asset('img/servis-hidropogon2.jpg') }}" alt="Pumpa za hidro pogon"/>
                        {{-- <a href="#" class="read-more fas fa-link"></a> --}}
                    </div><!-- /.img-box -->
                    <div class="content-box">
                        <div class="text-box">
                            <h3>Popravka hidro pogona</h3>
                        </div><!-- /.text-box -->
                    </div><!-- /.content-box -->
                </div><!-- /.single-service-style-one -->
            </div><!-- /.col-md-4 -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="single-service-style-one">
                    <div class="img-box">
                        <img src="{{ asset('img/servis-elektromotor.jpg') }}" alt="Elektromotor"/>
                        {{-- <a href="#" class="read-more fas fa-link"></a> --}}
                    </div><!-- /.img-box -->
                    <div class="content-box">
                        <div class="text-box">
                            <h3>Popravka elektromotora i generatora</h3>
                        </div><!-- /.text-box -->
                    </div><!-- /.content-box -->
                </div><!-- /.single-service-style-one -->
            </div><!-- /.col-md-4 -->
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="single-service-style-one">
                    <div class="img-box">
                        <img src="{{ asset('img/servis-plin.jpg') }}" alt="Ugradnja plina"/>
                        {{-- <a href="#" class="read-more fas fa-link"></a> --}}
                    </div><!-- /.img-box -->
                    <div class="content-box">
                        <div class="text-box">
                            <h3>Ugradnja i servisiranje plina</h3>
                        </div><!-- /.text-box -->
                    </div><!-- /.content-box -->
                </div><!-- /.single-service-style-one -->
            </div><!-- /.col-md-4 -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="single-service-style-one">
                    <div class="img-box">
                        <img src="{{ asset('img/servis-elektronika.jpg') }}" alt="Elektronika"/>
                        {{-- <a href="#" class="read-more fas fa-link"></a> --}}
                    </div><!-- /.img-box -->
                    <div class="content-box">
                        <div class="text-box">
                            <h3>Popravka elektronike</h3>
                        </div><!-- /.text-box -->
                    </div><!-- /.content-box -->
                </div><!-- /.single-service-style-one -->
            </div><!-- /.col-md-4 -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="single-service-style-one">
                    <div class="img-box">
                        <img src="{{ asset('img/mehatronika.png') }}" alt="Mehatronika"/>
                        {{-- <a href="#" class="read-more fas fa-link"></a> --}}
                    </div><!-- /.img-box -->
                    <div class="content-box">
                        <div class="text-box">
                            <h3>Popravka mehatronike</h3>
                        </div><!-- /.text-box -->
                    </div><!-- /.content-box -->
                </div><!-- /.single-service-style-one -->
            </div><!-- /.col-md-4 -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="single-service-style-one">
                    <div class="img-box">
                        <img src="{{ asset('img/servis-hidraulika.jpg') }}" alt="Hidraulika"/>
                        {{-- <a href="#" class="read-more fas fa-link"></a> --}}
                    </div><!-- /.img-box -->
                    <div class="content-box">
                        <div class="text-box">
                            <h3>Popravka hidraulike</h3>
                        </div><!-- /.text-box -->
                    </div><!-- /.content-box -->
                </div><!-- /.single-service-style-one -->
            </div><!-- /.col-md-4 -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="single-service-style-one">
                    <div class="img-box">
                        <img src="{{ asset('img/servis-dijagnostika.jpg') }}" alt="Alat za dijagnostiku viljuškara"/>
                        {{-- <a href="#" class="read-more fas fa-link"></a> --}}
                    </div><!-- /.img-box -->
                    <div class="content-box">
                        <div class="text-box">
                            <h3>Dijagnostika za Linda viljuškare</h3>
                        </div><!-- /.text-box -->
                    </div><!-- /.content-box -->
                </div><!-- /.single-service-style-one -->
            </div><!-- /.col-md-4 -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="single-service-style-one">
                    <div class="img-box">
                        <img src="{{ asset('img/viljuskar-servis.jpg') }}" alt="Viljuškar u magacinu"/>
                        {{-- <a href="#" class="read-more fas fa-link"></a> --}}
                    </div><!-- /.img-box -->
                    <div class="content-box">
                        <div class="text-box">
                            <h3>Nabavka i prodaja svih dijelova za viljuškare</h3>
                        </div><!-- /.text-box -->
                    </div><!-- /.content-box -->
                </div><!-- /.single-service-style-one -->
            </div><!-- /.col-md-4 -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="single-service-style-one">
                    <div class="img-box">
                        <img src="{{ asset('img/servis-kompresor1.jpg') }}" alt="Zračni kompresor"/>
                        {{-- <a href="#" class="read-more fas fa-link"></a> --}}
                    </div><!-- /.img-box -->
                    <div class="content-box">
                        <div class="text-box">
                            <h3>Popravka zračnih kompresora</h3>
                            <p>vicanih, rotacionih, klipnih</p>
                        </div><!-- /.text-box -->
                    </div><!-- /.content-box -->
                </div><!-- /.single-service-style-one -->
            </div><!-- /.col-md-4 -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="single-service-style-one">
                    <div class="img-box">
                        <img src="{{ asset('img/servis-kompresor2.png') }}" alt="Dio kompresora"/>
                        {{-- <a href="#" class="read-more fas fa-link"></a> --}}
                    </div><!-- /.img-box -->
                    <div class="content-box">
                        <div class="text-box">
                            <h3>Dijelovi za kompresore</h3>
                        </div><!-- /.text-box -->
                    </div><!-- /.content-box -->
                </div><!-- /.single-service-style-one -->
            </div><!-- /.col-md-4 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.service-style-one sec-pad -->

@endsection
