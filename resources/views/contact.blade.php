@extends('layouts.master')
@section('title')
    <title>Kontakt | Titanium Servis</title>
    <meta name="description" content="160">
    <link rel="canonical" href="{{ url('kontakt') }}" />

    <meta property="og:type" content="article" />
    <meta property="og:title" content="naslov" />
    <meta property="og:description" content="opis" />
    <meta property="og:image" content="LINK TO THE IMAGE FILE" />
    <meta property="og:url" content="{{ Request::url() }}" />
@endsection
@section('content')

<section class="inner-banner">
    <div class="container text-center">
        <h3>Kontakt</h3>
        <div class="breadcumb">
            <a href="{{ url('/') }}">Početna</a><!--
            --><span class="sep">-</span><!--
            --><span class="page-name">Kontakt</span>
        </div><!-- /.breadcumb -->
    </div><!-- /.container -->
</section><!-- /.inner-banner -->

<section class="contact-page-content sec-pad">
    <div class="container">
        @if (session()->has('success'))
            <div class="row alert alert-success col-md-8">
                Hvala vam što ste nam poslali poruku
            </div>
        @endif
        <div class="row">
            <div class="col-md-8">
                <form action="{{ url('kontakt') }}" method="POST" class="contact-form row">
                    @csrf
                    <div class="col-md-6">
                        <p>Ime i prezime</p>
                        <input type="text" name="name" required/>
                    </div><!-- /.col-md-6 -->
                    <div class="col-md-6">
                        <p>Email</p>
                        <input type="email" name="email" required/>
                    </div><!-- /.col-md-6 -->
                    <div class="col-md-6">
                        <p>Broj telefona</p>
                        <input type="text" name="phone" />
                    </div><!-- /.col-md-6 -->
                    <div class="col-md-6">
                        <p>Ime kompanije</p>
                        <input type="text" name="company" />
                    </div><!-- /.col-md-6 -->
                    <div class="col-md-12">
                        <p>Poruka</p>
                        <textarea name="message" required></textarea>
                        <button type="submit">Pošaljite nam poruku</button>
                    </div><!-- /.col-md-6 -->
                </form><!-- /.contact-form -->
                <div class="result"></div><!-- /.result -->
            </div><!-- /.col-md-8 -->
            <div class="col-md-4">
                <div class="contact-info">
                    <h3>Kontaktirajte nas</h3>
                    <p>Za još informacija imate <br /> detalje ispod.</p>
                    <div class="single-contact-info">
                        <i class="fas fa-phone"></i>
                        <a href="tel:+38763976331" style="color: black;">+387 63 976 331</a>
                    </div><!-- /.single-contact-info -->
                    <div class="single-contact-info">
                        <i class="fas fa-envelope-open"></i>
                        <a href="mailto:titaniumservis@gmail.com" style="color: black;">titaniumservis@gmail.com</a>
                    </div><!-- /.single-contact-info -->
                    <div class="single-contact-info">
                        <i class="fas fa-home"></i>
                        <p>Radoja Domanovića 12, <br /> Brčko, BiH</p>
                    </div><!-- /.single-contact-info -->
                    <div class="single-contact-info">
                        <i class="fas fa-id-badge"></i>
                        <p>JIB: 4700686130009</p>
                    </div><!-- /.single-contact-info -->
                </div><!-- /.contact-info -->
            </div><!-- /.col-md-4 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.contact-page-content -->

@include('components.map')

@endsection
