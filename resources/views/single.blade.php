@extends('layouts.master')
@section('title')
    <title>{{ $item->name }} | Titanium Servis</title>
    <meta name="description" content="160">
    <link rel="canonical" href="{{ url($item->type === 'forklift' ? 'viljuškari/'. $item->slug : 'kompresori/'. $item->slug) }}" />

    <meta property="og:type" content="article" />
    <meta property="og:title" content="naslov" />
    <meta property="og:description" content="opis" />
    <meta property="og:image" content="LINK TO THE IMAGE FILE" />
    <meta property="og:url" content="{{ Request::url() }}" />
@endsection
@section('content')

<section class="inner-banner">
    <div class="container text-center">
        <h3>Naziv artikla</h3>
        <div class="breadcumb">
            <a href="{{ url('/') }}">Početna</a><!--
            --><span class="sep">-</span><!--
            --><span class="page-name">Naziv artikla</span>
        </div><!-- /.breadcumb -->
    </div><!-- /.container -->
</section><!-- /.inner-banner -->

<section class="project-single-page sec-pad">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="img-box">
                    <img src="{{ asset($item->image_lg) }}"  alt="{{ $item->image_name }}"/>
                </div><!-- /.img-box -->
            </div><!-- /.col-md-7 -->
            <div class="col-md-5">
                <div class="single-project-content">
                    <h3>{{ $item->name }}</h3>
                    <p>{{ $item->description }}</p>
                </div><!-- /.single-project-content -->
            </div><!-- /.col-md-5 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.project-single-page sec-pad -->

@endsection
