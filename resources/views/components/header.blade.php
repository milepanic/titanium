<header class="header header-home-one">
    <div class="header-top">
        <div class="container">
            <div class="pull-left left-contact-info">
                <p><i class="fas fa-envelope"></i><a href="mailto:titaniumservis@gmail.com" style="color: white;">titaniumservis@gmail.com</a></p>
                <p><i class="fas fa-phone"></i><a class="text-light" href="tel:+38763976331" style="color: white;">+387 63 976 331</a></p>
                <p><i class="fas fa-map-marker"></i>Radoja Domanovica 12, Brčko</p>
            </div><!-- /.pull-left -->
            <div class="pull-right right-contact-info">
                <p><i class="fas fa-clock"></i>Ponedjeljak - Subota: 8:00 do 17:00</p><!--
                -->
            </div><!-- /.pull-right -->
        </div><!-- /.container -->
    </div><!-- /.header-top -->
    <nav class="navbar navbar-default header-navigation stricky">
        <div class="container clearfix">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".main-navigation" aria-expanded="false"><i class="text-danger fas fa-bars"></i></button>
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset('logo.svg') }}" width="170" height="160" alt="Titanium logo"/>
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse main-navigation mainmenu " id="main-nav-bar">

                <ul class="nav navbar-nav navigation-box">
                    <li @if(\Request::is('/')) class="current" @endif>
                        <a href="{{ url('/') }}">Početna</a>
                    </li>
                    <li @if(\Request::is('o-nama')) class="current" @endif>
                        <a href="{{ url('o-nama') }}">O Nama</a>
                    </li>
                    <li @if(\Request::is('usluge')) class="current" @endif>
                        <a href="{{ url('usluge') }}">Usluge</a>
                    </li>
                    <li @if(\Request::is('albumi')) class="current" @endif>
                        <a href="{{ url('albumi') }}">Albumi</a>
                    </li>
                    <li @if(\Request::is('viljuškari')) class="current" @endif>
                        <a href="{{ url('viljuškari') }}">Viljuškari</a>
                        {{-- <ul class="sub-menu">
                            <li> <a href="projects.html">Projects</a> </li>
                            <li> <a href="project-single.html">Projects Single</a> </li>
                            <li><a href="faq.html">FAQ Page</a></li>
                            <li><a href="team.html">Team</a></li>
                            <li><a href="testimonials.html">Testimonials</a></li>
                        </ul><!-- /.sub-menu --> --}}
                    </li>
                    <li @if(\Request::is('kompresori')) class="current" @endif>
                        <a href="{{ url('kompresori') }}">Kompresori</a>
                        {{-- <ul class="sub-menu">
                            <li> <a href="projects.html">Projects</a> </li>
                            <li> <a href="project-single.html">Projects Single</a> </li>
                            <li><a href="faq.html">FAQ Page</a></li>
                            <li><a href="team.html">Team</a></li>
                            <li><a href="testimonials.html">Testimonials</a></li>
                        </ul><!-- /.sub-menu --> --}}
                    </li>
                    <li>
                        <a href="{{ url('kontakt') }}" class="hidden-lg hidden-xl rqa-btn">Kontakt</a>
                    </li>
                    <!-- <li> <a href="contact.html">Kontakt</a> </li> -->
                </ul>
            </div><!-- /.navbar-collapse -->
            <div class="right-side-box">
                <a href="{{ url('kontakt') }}" class="rqa-btn">Kontakt</a>
            </div><!-- /.right-side-box -->
        </div><!-- /.container -->
    </nav>
</header><!-- /.header -->
