<div id='map' style='width: 100%; height: 600px; margin-bottom: 70px;'></div>
<script>
    mapboxgl.accessToken = 'pk.eyJ1Ijoiam92YW5qb3ZvIiwiYSI6ImNqcGl0cXVldzBvNW4zcW9oMTV3NzlsN28ifQ.eDB6z-H_z-xL6TrM8W9bnA';
    var map = new mapboxgl.Map({
        container: 'map',
        minZoom: 7,
        zoom: 13.3,
        style: 'mapbox://styles/mapbox/outdoors-v10',
        center: [18.80865,44.86634],
        scrollZoom: false
    });

    map.on("load", function () {
  /* Image: An image is loaded and added to the map. */
  map.loadImage("https://i.imgur.com/MK4NUzI.png", function(error, image) {
      if (error) throw error;
      map.addImage("custom-marker", image);
      /* Style layer: A style layer ties together the source and image and specifies how they are displayed on the map. */
      map.addLayer({
        id: "markers",
        type: "symbol",
        /* Source: A data source specifies the geographic coordinate where the image marker gets placed. */
        source: {
          type: "geojson",
          data: {
            type: "FeatureCollection",
            features:[{"type":"Feature","geometry":{"type":"Point","coordinates":[18.81102770221804,44.85879165081673]}}]}
        },
        layout: {
          "icon-image": "custom-marker",
        }
      });
    });
});
</script>
