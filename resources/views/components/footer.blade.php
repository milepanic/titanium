<footer class="site-footer">
    <div class="footer-top">
        <div class="container">
            <div class="row ">
                <div class="col-md-8 col-sm-6 col-xs-12">
                    <div class="footer-widget about-widget">
                        <a href="{{ url('/') }}"><img src="{{ asset('logo.svg') }}" width="150" height="180" alt="Titanium logo"/></a>
                        <p>
                            Titanium Servis
                        </p>
                        <div class="social">
                            <a href="https://www.instagram.com/titaniumservis" class="fab fa-instagram" target="_blank"></a>
                            <a href="mailto:titaniumservis@gmail.com" class="far fa-envelope"></a>
                            <a href="tel:+38763976331" class="fas fa-phone"></a>
                        </div><!-- /.social -->
                    </div><!-- /.footer-widget about-widget -->
                </div><!-- /.col-md-3 -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="footer-widget links-widget">
                        <div class="title">
                            <h3>Linkovi</h3>
                        </div><!-- /.title -->
                        <ul class="links-list">
                            <li><a href="{{ url('/') }}"><i class="fas fa-angle-right"></i>Početna</a></li>
                            <li><a href="{{ url('o-nama') }}"><i class="fas fa-angle-right"></i>O Nama</a></li>
                            <li><a href="{{ url('usluge') }}"><i class="fas fa-angle-right"></i>Usluge</a></li>
                            <li><a href="{{ url('albumi') }}"><i class="fas fa-angle-right"></i>Albumi</a></li>
                            <li><a href="{{ url('viljuškari') }}"><i class="fas fa-angle-right"></i>Viljuškari</a></li>
                            <li><a href="{{ url('kompresori') }}"><i class="fas fa-angle-right"></i>Kompresori</a></li>
                            <li><a href="{{ url('kontakt') }}"><i class="fas fa-angle-right"></i>Kontakt</a></li>
                        </ul><!-- /.links-list -->
                    </div><!-- /.footer-widget links-widget -->
                </div><!-- /.col-md-2 -->

                <!-- <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="footer-widget recent-post-widget">
                        <div class="title">
                            <h3>Recent Post</h3>
                        </div>
                        <div class="single-recent-post">
                            <div class="img-box">
                                <img src="img/footer-rp-1-1.jpg" alt="Awesome Image"/>
                            </div>
                            <div class="text-box">
                                <a href="#"><h4>China industrial profit growth slows to low</h4></a>
                                <p>5 hours ago</p>
                            </div>
                        </div>
                        <div class="single-recent-post">
                            <div class="img-box">
                                <img src="img/footer-rp-1-2.jpg" alt="Awesome Image"/>
                            </div>
                            <div class="text-box">
                                <a href="#"><h4>Industrial space to remain tight</h4></a>
                                <p>5 hours ago</p>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.footer-top -->
</footer><!-- /.site-footer -->
<div class="footer-bottom">
    <div class="container">
        <div class="copy-text">
            <p>
                Copyright &copy; 2018 | titaniumservis.com. All rights reserved.&nbsp;&nbsp;&nbsp;

                Web Development by
                <a href="https://www.laracode.net" target="_blank">
                    <img src="img/laracode-logo.svg" alt="" height="25">
                </a>
            </p>
        </div><!-- /.copy-text -->
        {{-- <div class="right-link pull-right">
            <p>
                Web Development by
                <a href="https://www.laracode.net" target="_blank">
                    <img src="img/laracode-logo.svg" alt="" height="25">
                </a>
            </p>
        </div> --}}
    </div><!-- /.container -->
</div><!-- /.footer-bottom -->
