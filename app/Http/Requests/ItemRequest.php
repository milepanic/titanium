<?php

namespace App\Http\Requests;

use App\Item;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'image' => 'required|image',
                    'imageName' => 'required|string',
                    'name' => 'required|string|min:3|max:255|unique:items,name',
                    'description' => 'nullable|string',
                    'type' => 'required|string',
                ];
            case 'PATCH':
            case 'PUT':
                $item = Item::whereName($this->name)->first();

                return [
                    'image' => 'nullable|image',
                    'imageName' => 'required|string',
                    'name' => 'required|string|min:3|max:255|unique:items,name,' . $item->id,
                    'description' => 'nullable|string',
                    'type' => 'required|string',
                ];
        }
    }
}
