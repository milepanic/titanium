<?php

namespace App\Http\Controllers;

use App\Album;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function welcome()
    {
        return view('welcome')->with(
            'albums', Album::latest()->take(8)->get()
        );
    }
}
