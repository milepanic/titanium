<?php

namespace App\Http\Controllers;

use App\Http\Requests\ItemRequest;
use App\Item;
use App\Services\ImageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type = null;

        if (request()->is('viljuškari')) {
            $type = 'forklift';
        } else if (request()->is('kompresori')) {
            $type = 'compressor';
        }

        return view('items')->with(
            'items', Item::whereType($type)->paginate(15)
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ItemRequest $request)
    {
        ImageService::store(
            $request->image, $request->imageName, 'proizvodi'
        );

        Item::create([
            'image_sm' => "storage/proizvodi/sm-$request->imageName.jpg",
            'image_lg' => "storage/proizvodi/lg-$request->imageName.jpg",
            'image_name' => $request->imageName,
            'name' => $request->name,
            'slug' => str_slug($request->name),
            'description' => $request->description,
            'type' => $request->type
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        return view('single')
            ->with('item', Item::whereSlug($slug)->first());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        return view('admin.edit-item')
            ->with('item', Item::whereSlug($slug)->first());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(ItemRequest $request, $slug)
    {
        $item = Item::whereSlug($slug)
            ->first();

        $item->update([
            'name' => $request->name,
            'slug' => str_slug($request->name),
            'description' => $request->description,
            'type' => $request->type
        ]);

        if ($request->image) {
            File::delete($item->image_sm, $item->image_lg);
            
            ImageService::store(
                $request->image, $request->imageName, 'proizvodi'
            );

            $item->update([
                'image_sm' => "storage/proizvodi/sm-$request->imageName.jpg",
                'image_lg' => "storage/proizvodi/lg-$request->imageName.jpg",
                'image_name' => $request->imageName,
            ]);
        }

        return redirect("admin/proizvodi/$item->slug/edit");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        Item::whereSlug($slug)->delete();

        return 200;
    }
}
