<?php

namespace App\Http\Controllers;

use App\Album;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class AlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('albums')->with(
            'albums', Album::latest()->paginate(8)
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.images-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $path = "albumi/$request->name";

        Storage::put(
            "public/$path/album-main-image.jpg", 
            Image::make($request->cover)
                ->fit(400)
                ->encode('jpg')
        );

        $names = [];

        foreach ($request->images as $image) {
            $name = $image->getClientOriginalName();

            Storage::putFileAs("public/$path", $image, $name);

            array_push($names, "storage/$path/$name");
        }

        Album::create([
            'name' => $request->name,
            'slug' => str_slug($request->name),
            'cover' => "storage/$path/album-main-image.jpg",
            'coverAlt' => $request->coverAlt,
            'images' => json_encode($names),
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        // return view('album')->with(
        //     'album', Album::whereSlug($slug)->first()
        // );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function edit(Album $album)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Album $album)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        Album::whereSlug($slug)->delete();

        return 200;
    }
}
