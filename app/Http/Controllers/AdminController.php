<?php

namespace App\Http\Controllers;

use App\Album;
use App\Http\Requests\ItemRequest;
use App\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class AdminController extends Controller
{
    public function dashboard() 
    {
        return view('admin.index');
    }

    // public function create() 
    // {
    //     return view('admin.add');
    // }

    // public function store(ItemRequest $request)
    // {
    //     $ext = $request->image->getClientOriginalExtension() == 'gif' ? '.gif' : '.jpg';

    //     $name = $request->imageName . '-' . uniqid() . $ext;
    //     Storage::putFileAs("public/proizvodi", $request->image, $name);

    //     Item::create([
    //         'image' => "storage/proizvodi/$name",
    //         'image_name' => $request->imageName,
    //         'name' => $request->name,
    //         'slug' => str_slug($request->name),
    //         'description' => $request->description,
    //         'category' => $request->category,
    //         'type' => $request->type
    //     ]);

    //     return back();
    // }

    public function items() 
    {
        if (request()->ajax())
            return DataTables::eloquent(
                Item::query())
                    ->addColumn('action', function ($item) {
                           return view('admin.components.datatables.items-action', compact('item'));
                       })
                    ->make(true);

        return view('admin.items');
    }

    // public function edit($slug) 
    // {
    //     return view('admin.edit-item')
    //         ->with('item', Item::whereSlug($slug)->first());
    // }

    // public function update(Request $request, $slug) 
    // {
    //     Item::whereSlug($slug)
    //         ->first()
    //         ->update([
    //             // 'image' => "storage/proizvodi/$name",
    //             // 'image_name' => $request->imageName,
    //             'name' => $request->name,
    //             'slug' => str_slug($request->name),
    //             'description' => $request->description,
    //             'category' => $request->category,
    //             'type' => $request->type
    //         ]);

    //     return redirect('admin/proizvodi');
    // }
    // 
    public function images() 
    {
        if (request()->ajax())
            return DataTables::eloquent(
                Album::query())
                    ->addColumn('action', function ($album) {
                           return view('admin.components.datatables.album-action', compact('album'));
                       })
                    ->make(true);

        return view('admin.images');
    }

}
