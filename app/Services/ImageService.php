<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

/**
 * 
 */
class ImageService
{
    public static function store($image, $name, $folder)
    {
        $nameSmall = "sm-$name.jpg";
        $nameLarge = "lg-$name.jpg";

        Storage::put(
            "public/$folder/$nameSmall", 
            Image::make($image)
                ->fit(370)
                ->encode('jpg')
        );

        Storage::put(
            "public/$folder/$nameLarge", 
            Image::make($image)
                ->fit(600)
                ->encode('jpg')
        );
    }

}
