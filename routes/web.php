<?php

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/', 'FrontController@welcome');
Route::view('o-nama', 'about');
Route::view('usluge', 'services');
Route::view('kontakt', 'contact');

Route::get('viljuškari', 'ItemController@index');
Route::get('viljuškari/{slug}', 'ItemController@show');

Route::get('kompresori', 'ItemController@index');
Route::get('kompresori/{slug}', 'ItemController@show');

Route::post('kontakt', 'ContactController@store');

Route::get('albumi', 'AlbumController@index');

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/', 'AdminController@dashboard');
    Route::get('dodavanje', 'ItemController@create');
    Route::post('dodavanje', 'ItemController@store');
    Route::get('proizvodi', 'AdminController@items');
    Route::get('proizvodi/{slug}/edit', 'ItemController@edit');
    Route::put('proizvodi/{slug}', 'ItemController@update');
    Route::delete('proizvodi/{slug}', 'ItemController@destroy');
    Route::get('kontakt', 'ContactController@index');
    Route::get('slike', 'AdminController@images');
    Route::delete('slike/{slug}', 'AlbumController@destroy');
    Route::get('slike/dodavanje', 'AlbumController@create');
    Route::post('slike/dodavanje', 'AlbumController@store');
});
